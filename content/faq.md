---
title: "FAQ"
date: 2021-12-18T22:06:28-05:00
draft: false
---

**Why don't you include exact measurements in most of your recipes?**

This recipe site was made for me. I don't use exact measurements unless it's
necessary.

Don't like it? Feel free to fork the site
[here](https://gitlab.com/dg-website/recipes) and add whatever exact
measurements you like.

**Ok well why don't you like exact measurements?**

I find recipes are better when there's a little variation to each recipe. At
some point you find out what tastes better and worse this way. Rather than
copying and pasting values, you begin to understand why something is the way it
is.

Likewise, these inexact measurements let you play around with how much or how
little goes into a recipe. You might accidentally find more or less of a certain
spice or ingredient goes good in something.

Besides, you already know how much chili powder you were going to add. Don't ask
me.

For video form of this check out [Internet Shaq's: A cooking skill that can't be
taught](https://www.youtube.com/watch?v=gYwkKaK5yeQ)

**No stories on your recipes blog?**

No stories, no ads. I'm not making money on this and I don't care about SEO.
This is for me and friends and family. If you found it and like it though, feel
free to enjoy `:)`

**Why don't you include as many long-form instructions on your site?**

I find the standard way recipes are represented online is just awful. 99% of the
time I look at ingredients as I'm about to use them.

Usually the instructions that are given after the the ingredients are so simple
they could just include it in the recipes list.

Most of the ingredients are also added in batches, but the ingredients are
always unstructured. Why do they do this? It's awful.

I set up my ingredients list in the batches they go in, and try to include
instructions in the batch names. For many of the recipes, after you make the
recipe once or twice, you won't even need the short form instructions I include
anyway.

The short form instructions and batched ingredients are usually enough.

**Are all of the recipes on your site vegetarian?**

No! Some are vegan! All recipes are vegetarian if not labeled as vegan though.

**Can I subscribe?**

No. I don't feel like maintaining an email list. Just look at the site when you
want a recipe. I'm doing my best to add one every week of the year in 2022.
