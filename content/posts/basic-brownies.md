---
title: "Basic Brownies"
date: 2023-08-14T23:27:42-04:00
draft: false
tags:
- american
- dessert
thumbnail: "/basic-brownies/awful-photo.jpg"
---

![The finished product](/basic-brownies/awful-photo.jpg)

Looking for above average brownies for super cheap and you can make a gazillion
at once? You're in the right place.

The photo is awful because I never take photos of these. I make a billion and
send them off right away. You'll like them though.

# Recipe

### The wet ingredients
* 1/2 cup of melted butter
* 1 cup of sugar (why is this wet idk)
* 2 eggs
* 1 tsp of vanilla extract

### Everything else
* 1/3 cup of unsweetened cocoa powder
* 1/2 cup of flour
* 1/4 tsp of salt
* 1/4 tsp of baking powder
* 1/2 a bag of chocolate chips (semi-sweet or dark chocolate)

# Instructions
* Preheat oven to 350°F
* Melt the butter and make sure it cools a little
* Throw all of the wet ingredients in a mixer
* Throw all of the dry ingredients in a mixer until smooth
* Throw in a 9x9 pan
* Put in the oven for 25-30 minutes (maybe longer, do the toothpick test)
* Eat

# Addendum

Shamelessly stolen from [allrecipes](https://www.allrecipes.com/recipe/10549/best-brownies/).

You don't need the frosting. It's not worth it.
