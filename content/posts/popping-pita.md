---
title: "Popping Pitas"
date: 2022-06-05T21:58:38-04:00
draft: false
tags:
- mediterranean
- vegan
- medium
- main course
- side course
- standard dough
thumbnail: "/popping-pita/popping-puffs.jpg"
---

![Some fresh and tasty pita bread](/popping-pita/popping-puffs.jpg)

# Recipe

### Pre-flour
* 2 1/2 tsp of active dry yeast
* 1 2/3 cups of warm water (toasty)
* 1 tsp of granulated sugar or some honey

### Flour and everything else
* 1 1/4 tsp salt
* 4 cups of flour

# Instructions

### Prep
* Put the pre-flour stuff in the mixer bowl
* Mix it all up and wait for it to bubble a little (5 mins)
* Add a cup of flour and begin mixing
* Slowly add the rest of the flour
* Add the other stuff from flour and everything else
* Add a little extra flour until it doesn't stick to the bowl
* Cover the bowl in foil and let it sit for 2 hours
* Take out the dough and press flat with your hands
* Cut it into 2ish inch pieces
* Fold into little balls and place on a tray, let sit for 30 minutes in the
  fridge (about 20 pieces)

### Cooking
* Preheat oven to 450 (for less puff, set to as low as 400, the higher the heat,
  the puffier)
* Put cooking stone into oven and preheat with the oven
* Press dough balls flat into circles with a rolling pin
* Put in the oven for 4-5 minutes
* Flip each partially cooked pita and cook for an additional 2-3 minutes
* Take out and let sit under a towel for 10 minutes
* Gobble

# Addendum

These come out tender and tasty. Nice and chewy. They're very solid and hold
food very well in little pockets.

Best eaten warm, but if you can't preserve the dough make em all at once.

![The pita with a little fluff in the pan](/popping-pita/tiny-puffs.jpg)

Don't like white grain? You can make them some whole grain. I don't recommend
going over a ratio of 50% white to whole grain. White flour pitas are much
chewier and puff a little better.

1/4 whole grain to 3/4 white is the best ratio if you want to add some whole
grains. You'll barely notice they're there.

With that said, you can make them 100% whole grain like I did here:

![Less fluffy and popping, but still good!](/popping-pita/whole-grain.jpg)

Classical pitas, hummus, and some meat substitute with some vegetables:

![Classic pita pockets](/popping-pita/hummus-pita.jpg)

My girlfriend really likes making little pizzas out of them:

![Pizza pita](/popping-pita/pizza-time.jpg)

# Adapted from

[Fluffy Homemade Pita
Bread](https://www.lionsbread.com/fluffy-homemade-pita-bread/)

[Pita Bread - How to Make Pita Bread at Home - Grilled
Flatbread](https://www.youtube.com/watch?app=desktop&v=NPiA69p4gqE)
