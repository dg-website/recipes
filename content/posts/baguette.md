---
title: "Baguette"
date: 2022-07-04T14:43:58-04:00
draft: false
tags:
- french
- vegan
- medium
- standard dough
- side
thumbnail: "/baguette/baguette.jpg"
---

![The finished product](/baguette/baguette.jpg)

# Recipe

### Pre-flour
* 2 1/2 tsp of active dry yeast
* 1 2/3 cups of warm water (95 degrees/warm)
* 1 tsp of granulated sugar

### Flour and everything else
* A smidge of olive oil
* 2 tsp salt
* 4 cups of flour

# Instructions

### Prep
* Put the pre-flour stuff in the mixer bowl
* Mix it all up and wait for it to bubble a little (5 mins)
* Add a cup of flour and begin mixing
* Slowly add the rest of the flour
* Add the other stuff from flour and everything else
* Add a little extra flour until it doesn't stick to the bowl
* Cover the bowl in foil and let it sit for 2 hours
* Take out the dough and press flat with your hands on lightly greased cutting
  board
* Cut into three pieces
* Round the pieces each into a rough ball by pulling the edges to the center
* Cover with greased plastic and leave in the fridge for 15-60 mins

### Prep 2
* Flatten the dough slightly from one of the dough balls with your hands
* Rotate 180 degrees and pinch inward from side to side
* Press down and flatten slightly, then pinch inward again (not rotated this
  time)
* Roll into 16 inch logs
* Place seam side down on a lightly greased pan with lightly greased plastic
  covering
* Leave until puffy at room temperature (68F, about 45 min)

### Actually cooking the damn thing
* Preheat oven to 450F
* Cut a few slits at a 45 degree angle into each of the dough with a sharp knife
* Bake for 24 minutes (slightly longer for extra crunchy exterior)

# Adapted from

[King Arthur Flour - classic baguette recipe](https://www.kingarthurbaking.com/recipes/classic-baguettes-recipe)

Support King Arthur Flour by buying their flour! It's super high quality and
co-op run!

# Addendum

For storage:
* Overnight they can be left in a paper bag
* Otherwise freeze for longer storage.

Ok yes these are not real baguettes, but they're pretty close leave me alone.
