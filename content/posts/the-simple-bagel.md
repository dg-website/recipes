---
title: "The Simple Bagel (and fries!)"
date: 2022-01-06T21:20:44-05:00
draft: false
tags:
- easy
- american?
- main-course
thumbnail: "/the-simple-bagel/simple-bagel.jpg"
---

![A bagel and fries](/the-simple-bagel/simple-bagel.jpg)

# Description

Some days all you have left is a bagel, some stuff in the fridge, and a potato.
Let's make something tasty out of it.

# Recipe

### The fries ingredients

* One potato (sweet or not, whatever you like)
* A little oil
* Seasoned salt, or whatever your favorite seasonings are

### The bagel ingredients

* Your favorite bagel (or whatever is left over)
* Some leftover vegetables
* Some hummus
* Any dressings, salsas, queso, teriyaki sauce, whatever else you have
* A little oil
* Perhaps a meat free burger if you're feeling wild

# Preparation

## The potato
Chop the potato into large pieces and boil for about 10 minutes. Drain the water
and add some oil and a LOT of your seasonings of choice. It should really be
coated.

Throw it in an air fryer for 6 minutes. Shake and do it again until the potatoes
look crunchy. You'll know what I mean when you see it.

## The bagel thing

Toast your bagel or not, I don't care. Saute your vegetables and add any of your
saute sauce if applicable. Toast up your meat free burger if you have it. Add
your sauces and everything else to the bagel and you're done.

## I don't have an air fryer

![😏](/the-simple-bagel/air-fryer.jpg)

A dumb post deserves a dumb meme. Just throw your potatoes sticks in the oven
for a little while. No idea how long. When they look crispy they're probably
done.
