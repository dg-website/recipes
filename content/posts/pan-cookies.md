---
title: "Pan Cookies"
date: 2023-06-04T13:54:04-04:00
draft: false
tags:
- easy
- dessert
thumbnail: "/pan-cookies/cookies.jpg"
---

![The finished product](/pan-cookies/cookies.jpg)

# Recipe

### Prep
* 350°F
* With 9x12 cooking sheet (parchment paper lined)

### Dry ingredients to mix first
* 2 1/4 cups of flour
* 1 tablespoon of cornstarch
* 1 teaspoon of baking soda
* 1 teaspoon of salt

### Secondary ingredients to mix in a separate bowl
* 1 cup of melted (unsalted) butter
* 1 cup of brown sugar
* 3/4 cup of granulated sugar
* 1 teaspoon of vanilla extract (optional, better with)
* 2 eggs

### The flour to add near the end
* 2 1/4 cups of flour

### The chocolate chips at the very end
* 2 cups of chocolate chips

# Instructions
* Whisk together the dry ingredients to mix first in its own bowl
* Mix together the secondary ingredients the mixer bowl
  * Make sure the butter is not too hot
* In a mixer, combine the dry and secondary ingredients
* Add the flour bit by bit
* Add the chocolate chips at the very end
* Put in the 9x12 pan and cook for 38 minutes in the oven at 350°F
  * You can spread it across two pans and cook at for 20 minutes if preferred

# Adapted from
* [This ad laden website - sugar and soul](https://www.sugarandsoul.co/chocolate-chip-pan-cookies/)
