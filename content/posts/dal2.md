---
title: "Instant pot dal 2"
date: 2022-07-23T21:25:07-04:00
draft: false
tags:
- indian
- vegan
- easy
- main course
thumbnail: "/dal2/dal2.jpg"
---

![The finished product](/dal2/dal2.jpg)

If [dal](/easy-peasy-dal-instant-pot) is so good, why didn't they make a dal 2?

Well guess what, they did. This one is less tomatoey and more authentic.

# Recipe

### Stuff to saute
* Some oil
* 3/4 tsp cumin seeds
* One small red onion
* A gallon of garlic cloves
* Some green chili
* 1 medium tomato

### Everything else

* 1 cup toor dal
* 3 cups of water
* Little bit of turmeric
* Salt to taste
* Little bit of lemon juice
* 2 cups of baby spinach (optional but tasty)

# Instructions

* Saute the stuff in the stuff to saute section 1 at a time
* Put the everything else section except for spinach in the pot
* Pressure cook on the instant pot for ~15 minutes
* Add the spinach and hit the saute a little
* Add extra water if desired
* Enjoy with some basmati rice

# Adapted from

[Instant pot rice with dal](https://www.cookwithmanali.com/instant-pot-rice-and-dal/)
