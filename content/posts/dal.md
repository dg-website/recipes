---
title: "Easy peasy dal (Instant Pot)"
date: 2021-12-16T22:06:28-05:00
draft: false
tags:
- indian
- vegan
- easy
- main course
thumbnail: "/easy-peasy-dal/dal.jpg"
---

![The finished product](/easy-peasy-dal/dal.jpg)

# Recipe

### What to saute with
* A tablespoon or two of butter or ghee

### Stuff to saute
* About one onion
* About one pepper
* One chili pepper (as spicy as you want)

### Stuff to add after sauteing
* A can of diced tomatoes (no salt added) or like 2 tomatoes
* About a cup of toor dal
* About a cup of masoor dal

### Spices to add once you've added the beans
For each of these, a good sized spoonful is best (add as much as you like
though)
* Garam masala
* Kashmiri chili powder (or regular chili powder if boring)
* Turmeric
* Cumin
* A small spoonful of salt

### Everything else
* 5 cups of water

# Instructions

* Chop the vegetables in the "stuff to saute" section.
* Turn your instant pot onto the saute mode and put the vegetables in (know
  for how long)
* Add the stuff to add after sauteing
* Add everything in the spices to add blah section
* Add "everything else"
* Stir a little
* Set your instant pot to pressure cook mode for 14 minutes
* Ok now you're done

# I don't have an instant pot

When you get to the instant pot section, instead of pressure cooking let
everything simmer for a while in a regular pot with the lid on until it's all
tender. I've never done it this way, but I'd guess it'll probably be an hour or
two of time before it's done.



