---
title: "Nama Style Chocolate"
date: 2021-12-23T23:45:37-05:00
draft: false
tags:
- japanese
- dessert
- chocolate
- easy
thumbnail: "/nama-style-chocolate/chocolates.jpg"
---

![Tasty chocolates, some ready to go, some ready to be
sliced](/nama-style-chocolate/chocolates.jpg)

# Description

"Home made" chocolates that are exceptionally soft. They're slightly less sweet
and much richer than "standard" American chocolate. Most popularly sold by a
company called Royce, this recipe costs as much as 15x less depending on where
you live.

# Recipe

### To melt/heat

* 2-3 tbsp of unsalted butter (must be real butter)
* 1/4-1/3 cup of whipping cream

### The chocolate

* A bag of chocolate chips (60+% dark chocolate, apx 1.5 cups)

### Optional but recommended

* A little cocoa powder for sprinkling

# Preparation

* Melt/heat the ingredients in the "to melt/heat" section (until bubbling)
* Add the chocolate chips until they too are melted (may need to continue
  heating)
  * If you move everything to a bowl, you can use the microwave (do in 30s
    increments)
* Pour into a mold or molds. The taller your chocolate is, the longer it will
  have to sit
  * Line the sides with parchment paper. Be careful it is smooth, otherwise your
    chocolate will look weird
* Leave mold in the fridge for 3-5 hours
* Slice into individual pieces as large or small as you like
  * For optimal chocolate, slices, rinse the knife under hot water after each cut
  * For good enough chocolate slices, you can rinse every 3-5 slices
* Dust with cocoa powder

# Storage

For longest shelf life, keep in the fridge when not eating and take out an hour
or two before you're going to eat some.

It probably won't last very long though ;)

# Adapted from

[Nino's House: Nama Chocolate [Only 3
Ingredients]](https://www.youtube.com/watch?v=GzmlT1rHxfM)
