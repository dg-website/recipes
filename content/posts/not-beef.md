---
title: "Not Beef"
date: 2022-04-24T15:26:50-04:00
draft: false
tags:
- american
- vegan
- easy
- main course
thumbnail: "/not-beef/not-beef.jpg"
---

![Fake meat time](/not-beef/not-beef.jpg)

# Recipe

### Stuff to saute
* A bit of your favorite oil
* About one onion (pearled onions if you feel ritzy)
* A bunch of peas
* [Gardein "beef" tips](https://www.gardein.com/beefless-and-porkless/classics/bef-tips)
* Some kind of meat marinade (I used a peppercorn one I found at target)

# Instructions

It's really easy. Chop your onions and saute them. Then add your fake beef tips.
Then add everything else.

Saute long enough so the marinade is not runny.

Tastes good with potatoes.

# FAQ

Q: I want those thicc fries, how do I make them

A: Check out [the simple bagel (and fries!)](/the-simple-bagel-and-fries/#the-potato)
