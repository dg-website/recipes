---
title: "Chickpea Salad"
date: 2024-07-27T20:52:14-04:00
draft: false
tags:
- mediterranean
- side
- easy
- main course
thumbnail: /chickpea-salad/chickpea-salad.jpg
---

![The finished product](/chickpea-salad/chickpea-salad.jpg)

Q: Did you really leave the plastic on the bowl in the photo?

A: Yeah, the recipe is still good though.

It's quick and easy and tastes good and it's healthy. Can't go wrong. Good on a
hot day too.

# Recipe

### The stuff in the bowl
* Two 15 oz cans of chickpeas (drained and rinsed)
* A chopped up tomato or two (cherry tomatoes are better, but more expensive)
* A chopped up cucumber
* A chopped up red onion
* Half a block of feta cheese (or as much as you want)
* [Optional] Chopped up avocado

### The dressing
* A reasonable amount olive oil (don't skimp)
* Less half of that in lemon juice
* Some salt
* Some pepper
* Some minced garlic

# Instructions

* Take the stuff in a bowl and put it in a bowl
* Take the dressing stuff and put it in a bowl. Give it a good stir
* Mix everything up and you're done


# Bonus tips
* Leaving it in the fridge for a bit will make it taste better
* Eat with rice, roasted potatoes, or pita breads

# Adapted from

[Chickpea Salad Recipe](https://natashaskitchen.com/chickpea-salad-recipe/)
