---
title: "Instant Pot Home Made Tomato Soup"
date: 2022-01-18T20:33:00-05:00
draft: false
tags:
- italian?
- vegan
- main course
- medium
thumbnail: "/home-made-tomato-soup/soup.jpg"
---

![Soup](/home-made-tomato-soup/soup.jpg)

# Description

Come on man it's tomato soup. What do you want me to write here. I just got a
gum graft so the photos aren't great. Oh well.

# Recipe

## For sauteing

* One whole onion
* A dollop of your favorite cooking oil (olive? corn? whatever you like)
* Other vegetables if you really want, we're pureeing it after so it doesn't
  matter

## Everything else

* One of those 28 oz cans of diced tomatoes
* A can of tomato paste
* All of your favorite tomato spices (basil, parsley, thyme, whatever)
* 2 cups of vegetable stock

# Preparation

Slice and dice the onion and any other vegetables you want to add. Put oil in
the instant pot and turn on saute. Then saute your vegetables.

Put everything else from the "everything else" section in, then pressure cook on
high for 15 minutes.

Once done, use an immersion blender and puree it all.

You're done. Add pasta or grilled cheese, or whatever you like to have with your
tomato soup.

# I don't have an instant pot

Just leave it on the stove at a really low temperature for a while until
everything is super soft.

# I don't have an immersion blender

Do you have a regular blender? If you do, wait for it to cool a little and then
put it in that. Otherwise you're going to need one.
