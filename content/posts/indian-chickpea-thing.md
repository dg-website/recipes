---
title: "Indian Chickpea Thing"
date: 2023-08-14T23:09:09-04:00
draft: false
tags:
- indian
- easy
- main course
thumbnail: "/indian-chickpea-thing/indian-inspired-bean-thing.jpg"
---

![The finished product](/indian-chickpea-thing/indian-inspired-bean-thing.jpg)

# Recipe

### The bean

* One of those really large cans of chickpeas

### The spices part 1

* Also olive oil because it makes sense here
* Cumin seeds

### The wet ingredients

* BBQ sauce
* Whole fat Indian yogurt
* Tomato paste
* A tablespoon of butter

### The rest of the spices
* Kashmiri chili powder (to flavor)
* Crushed Fenugreek seeds (a little bit)
* Garam masala (Less than the chili powder, but more than the fenugreek seeds)

### The paneer

* About 1/4th a cube of paneer chopped up

# Instructions

* Drain "the bean" and rinse, put on a plate and microwave for 5-6 minutes (I'm
  not kidding)
* Coat a pan with hot olive oil and then add the chickpeas (it should crackle)
* Add the cumin seeds, and saute until aromatic and chickpeas look a little
  crispy
* Add enough bbq sauce to coat the chickpeas
* Add a large scoop of yogurt and stir in
* Add a large scoop of tomato paste and stir in
* Keep doing previous 2 steps until reasonably creamy
* Add in a tablespoon of butter and let melt
* Add the rest of the spices
* Add the paneer
* All done

# Addendum

Great with [home-made pitas](/popping-pitas). Fill em up or use them like rotis.

Works with butterbeans too for something incredibly creamy. Just don't microwave
[the](the) butterbeans.

Makes enough for somewhere between 3 and 4 people.

![Butterbean version](/indian-chickpea-thing/with-butter-beans.jpg)

[Heavily adapted from Internet Shaquille's chickpea 2 video](https://www.youtube.com/watch?v=5EU76q3Vf3Q)
