---
title: "Air fried Gobi Manchurian"
date: 2022-01-07T22:34:52-05:00
draft: false
tags:
- indian
- vegan
- medium
- main course
- side course
- hall of fame
thumbnail: "/gobi-manchurian/ultimate-gobi.jpg"
---

![This stuff is GOOD](/gobi-manchurian/ultimate-gobi.jpg)

# Description

TL/DR this is air fried, breaded, cauliflower with some GOOD sauce. This is one
of my favorites and gets the first "Hall of fame" tag. Any more favorites will
go there.

# Recipe

### The cauliflower

* A loaf of cauliflower

### The cauliflower breading

* A half a cup of flour (whole wheat if you feel healthy)
* A fourth a cup of corn starch
* A little bit of oil
* About a half a cup of water

### The saucy stuff

* A half a cup of keptchup
* A reasonable amount of soy sauce
* A little bit of vinegar (whatever your favorite kind is)
* Kashmiri chili powder (a big old scoop)

### You'll want to saute this

* Half an onion
* A habanero pepper
* A bell pepper
* Some minced garlic
* Any other vegetables you want to add (spinach? peas? the world is your oyster)

# Preparation

Prepare the breading by putting it all in a bowl. It should be somewhere
between a liquid and a solid. Add more water if it's too thick.

Chop the cauliflower and boil it for 3 minutes. Coat it in the breading. Then
put it in the air fryer for 6 minutes and do it again until it looks and tastes
crunchy.

Prepare the saucey stuff by putting it all in a bowl.

Saute all of the vegetables in a big pan with some oil. Throw your air fried
cauliflower in the pan and add the sauce until it isn't "wet."

## I don't have an air fryer

See the recipe this was adapted from below for non-air-fryer instructions.

# Adapted from

[Ministry of curry, gobi
manchurian](https://ministryofcurry.com/gobi-manchurian/#recipe)

# More photos

I'm getting hungry again

![With some peas (why not) and air fried potatoes](/gobi-manchurian/gobi.jpg)

![Food in progress](/gobi-manchurian/gobi-in-progress.jpg)
