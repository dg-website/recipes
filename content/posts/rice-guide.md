---
title: "Rice Guide"
date: 2022-04-24T15:09:32-04:00
draft: false
tags:
- guide
- easy
thumbnail: "/rice-guide/rice.jpg"
---

![It's a photo of rice](/rice-guide/rice.jpg)

The first guide here. I'm tired of having to remember all of the different times
to cook rice for. This guide keeps track of how long to cook each type. I'll add
more as they appear.

For all of them, add 1x rice to 2x water ratio and a smidge of oil.

Cooking times are instant pot pressure cook on high times.

# Rices

* Brown : 23 minutes
* Basmati : 12 minutes

