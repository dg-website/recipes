---
title: "Taco salad"
date: 2022-02-15T22:26:28-05:00
draft: false
tags:
- mexican
- easy
- main course
- vegan
thumbnail: "/taco-salad-time/taco-salad.jpg"
---

![The finished product](/taco-salad-time/taco-salad.jpg)

# Description

A healthy blend of a ton of vegetables, beans, and some fake meat. You'll get
your veggies here without breaking a sweat.

# Recipe

### Initial saute ingredients

* A decent amount of oil (there's a lot of stuff in here)
* A whole onion (LARGE)
* An equal amount of bell pepper to the onion

### Saute ingredients part 2

* Some fake meat (boca crumbles are vegan)

### The rest

* A huge can of beans per onion
* Taco seasoning (1 pack per huge onion)

# Instructions

The recipe could not be easier. Once everything is chopped, go through each of
the sections and saute.

Saute the initial ingredients until the onions are sweet and peppers are tender.

Saute part 2 until the fake meat isn't frozen.

Put the rest in the pot and run on a low heat until everything is a nice
consistency.

# Extras

Add some rice and chopped romaine lettuce with the food and you can get a LOT of
food out of not so much money.

Sour cream and cheese make for nice additions too (both vegan in the photo
above). You can also package the food up in a wrap too for your own chipotle
burrito tailored to your macros.

Tortilla chips are a nice addition too.

