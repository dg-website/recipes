---
title: "Easy Banana Bread"
date: 2022-04-24T15:15:09-04:00
draft: false
tags:
- american?
- easy
- dessert
- Vegan (optional)
thumbnail: "/ez-banana-bread/bananas.jpg"
---

![Banana bread](/ez-banana-bread/bananas.jpg)

# Recipe

### Throw it all in a mixer

* 3 ripe bananas
* A few spoonfuls of peanut butter
* 1/4 cup of melted butter
* Some soy milk
* 1/4 cup of sugar (brown or white whatever)
* 1/4 cup of tiny chocolate chips
* 1.5 cups of whole wheat flour
* 3/4 tsp of baking soda (don't pick the wrong one)
* A smidge of salt
* 2 tsp of vanilla extract


# Instructions

* Put it all in a mixer
* Add as much soy milk as you need to moisten the mixture
* Set the oven to 350
* Grease a small pan and put the mixture in
* Leave in the oven for 1 hour

# Adapted from

[BEST Eggless Banana
Bread](https://mommyshomecooking.com/best-eggless-banana-bread/)
