---
title: "That easy saute"
date: 2022-02-01T21:31:51-05:00
draft: false
tags:
- college
- tired
- main course
- easy
thumbnail: "/easy-saute/saute.jpg"
---

![It's a saute](/easy-saute/saute.jpg)

# Description

Man sometimes you're tired but you don't just want a sandwich. Let's just make
something easy. It's just a saute of $STUFF. WE'VE ALL MADE THIS BEFORE DON'T
JUDGE.

# Recipe

### Saute round 1

* A few tablespoons of oil
* A whole pepper
* Maybe a chili pepper
* A whole onion

### Saute round 2

* I dunno man just get a bunch of vegetables idk spinach and broccoli are good
* Corn or peas are nice
* Some protein-type base (fake meat, beans, whatever you want)
* A good sauce: teriyaki, soy, a white or red sauce, whatever you think will
  taste good

# Instructions

Chop your veggies in saute round 1 and then saute them in a pan until soft. Then
throw everything else from saute round 2 in except the sauce. When everything is
rather tender, put your sauce of choice in and lower the temperature. When it
looks good you're done.

# Another photo

![It's a wrap](/easy-saute/its-a-wrap.jpg)

I didn't feel like making rice, but it's real good with rice too.

Frozen veggies make this really cheap and easy.

![I was hungry and forgot to take a photo of this before I  took
some](/easy-saute/stirfry2.jpg)
