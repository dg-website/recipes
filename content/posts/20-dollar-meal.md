---
title: "20 Dollar Meal"
date: 2022-02-10T19:02:19-05:00
draft: false
tags:
- italian
- easy
- main course
thumbnail: "/20-dollar-meal/20-dollar-meal.jpg"
---

![The finished product](/20-dollar-meal/20-dollar-meal.jpg)

# Description

You know the kind of meal you'd spend $20 for at an Italian restaurant. It costs
a fraction of the price to make and you get a gazillion servings. Included side
of veggies. [Eat your vegetables folks, otherwise you'll
die](https://arstechnica.com/science/2022/01/even-before-covid-americans-were-failing-at-health-basics-diet-exercise/).

# Recipe

### The pasta

* Your favorite ritzy pasta (the pricier one is better yes)
* pinch of salt

### The to saute before the sauce

* Some oil
* Half an onion (large)
* A gallon of vegetables (spinach, broccoli, a whole pepper, a chili pepper
  perhaps, your pot should be like half vegetables at the end)
* Garlic (however much you like)

### The sauce

* Just buy it, get one of the pricier ones if you can, they are better
* Your favorite spices (basil, chili powder, etc.)

### That tasty looking brussel sprout side
* Frozen brussel sprouts
* The other half of that onion you cut up
* Some oil
* Perhaps some cheese if you like
* Perhaps a little soy sauce of teriyaki sauce if you're feeling adventurous.

# Instructions
Throw your pasta+salt in a pot of boiling water for as long as the package says.
LISTEN TO HOW LONG IT SAYS !!! Then drain.

Add the stuff to saute before the sauce with some oil and saute until everything
looks tender.

Add the sauce ingredients and let it sit for a tiny bit on low heat (worth it I
promise), then add your pasta.

To make the vegetable side, saute your onion half first. When tender add
everything else and lower the heat a tiny bit. When that's tender too, you're
done. If you want to add cheese, put that on at the very end for just a few
minutes.

# Commentary

Q: This looks longer and possibly more difficult than other easy recipes, why is
this an easy recipe Darrien?

A: At the end of the day for every part of the recipe you're just throwing stuff
in a pan or pot and letting it either boil or saute for a little bit.

Once you've chopped everything it's basically just a waiting and stirring game.
Put on your favorite show and just stir it around a little while you wait.
